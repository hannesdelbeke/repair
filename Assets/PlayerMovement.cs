﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour
{

    [Header("Camera")]
    Camera m_cam;
    public Vector3 CamOffset;
	public float MaxDistanceDeltaCameraMovement = 0.5f;

    Rigidbody m_body;

    public float MovementForce = 200;

	public int PlayerNumber = 1;

    private Vector3 lookAtpos;

    private void Awake()
    {
        m_body = GetComponent<Rigidbody>();
    }

    // Start is called before the first frame update
    void Start()
    {
		if(SharkManager.s_instance)
			SharkManager.s_instance.AddHuntTarget(m_body);

        m_cam = Camera.main;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float hor = Input.GetAxis("Horizontal" + PlayerNumber.ToString());
        float vert = Input.GetAxis("Vertical" + PlayerNumber.ToString());

        if(Mathf.Abs(hor) + Mathf.Abs(vert) > 0.0f)
        {
            m_body.AddForce(Vector3.forward * vert * MovementForce);
            m_body.AddForce(Vector3.right * hor * MovementForce);
            transform.LookAt(transform.position + new Vector3(hor, 0.0f, vert));
        }

        if(PlayerNumber == 1 && m_cam)
        {
            m_cam.transform.position = Vector3.MoveTowards(m_cam.transform.position, GameLogic.s_instance.CurrentPlayerPos + CamOffset, MaxDistanceDeltaCameraMovement);
        }
    }
}
