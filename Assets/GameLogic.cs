﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLogic : MonoBehaviour
{

    public enum GameState
    {
        Menu,
        Playing,
        Win,
    }
    public GameState currentState;

    public GameObject PlayerPrefab;

    private List<GameObject> m_currentPlayers;

    public Vector3 CurrentPlayerPos { get { return CalculatePlayersCenterPoint(); } private set {} }

    public GameState StartingState;

    public Transform SpawnPoint;

    public int WinningAmountOfPlanks = 5;

    public static GameLogic s_instance;

    public AudioSource BackingMusic;

	public int NumOfPlayers = 1;
    public Material[] PlayerColours;

    private int winningPlayer;

    private AudioSource winSound;

    void Awake()
    {
        s_instance = this;
        BackingMusic = GameObject.FindGameObjectWithTag("BackingMusic").GetComponent<AudioSource>();
		m_currentPlayers = new List<GameObject>();
        winSound = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        ChangeState(StartingState);
    }

    // Update is called once per frame
    void Update()
    {
        switch(currentState)
        {
            case GameState.Menu:
            {
                if(Input.GetButtonDown("Jump1"))
                {
                    ChangeState(GameState.Playing);
                    return;
                }
                break;
            }
            case GameState.Playing:
            {
                break;
            }
            case GameState.Win:
            {
                if(Input.GetButtonDown("Jump1"))
                {
                    ChangeState(GameState.Menu);
                    return;
                }
                break;
            }
        }  
    }

    void ChangeState(GameState newState)
    {
        switch(newState)
        {
            case GameState.Menu:
            {
                UISystem.s_instance.ShowTitleScreen(true);
				UISystem.s_instance.ShowGameScreen(false);
                UISystem.s_instance.ShowWinScreen(false, 0);
                if(m_currentPlayers.Count > 0)
                {
                    Destroy(m_currentPlayers[0]);
                }
                break;
            }
            case GameState.Playing:
            {
                UISystem.s_instance.ShowTitleScreen(false);
				UISystem.s_instance.ShowGameScreen(true);
				RaftManager.s_instance.ResetRafts();
                SpawnPlayer();
                break;
            }
            case GameState.Win:
            {
                UISystem.s_instance.ShowGameScreen(false);
                UISystem.s_instance.ShowWinScreen(true, winningPlayer);
				if (m_currentPlayers.Count > 0)
				{
                    Destroy(m_currentPlayers[0]);
                }
                break;
            }
        }  
        currentState = newState; 
    }

    void SpawnPlayer()
    {
		//ADD MULTIPLAYER HERE
		for(int i = 0; i < NumOfPlayers; ++i)
		{
			m_currentPlayers.Add(Instantiate(PlayerPrefab, SpawnPoint.position + (Vector3.left * i), SpawnPoint.rotation));
			m_currentPlayers[i].GetComponent<PlayerMovement>().PlayerNumber = i + 1;
			m_currentPlayers[i].GetComponent<Grabber>().PlayerNumber = i + 1;
             Material[] mats = new Material[]{PlayerColours[i]};
           
            m_currentPlayers[i].GetComponentInChildren<SkinnedMeshRenderer>().materials  = mats;
		}
    }

    void GameOver(bool won)
    {
        if(won)
        {
            Debug.Log("You Win");
        }
        else
        {
            Debug.Log("You Lose");
        }
        winSound.Play();
        ChangeState(GameState.Win);
    }

    public void CheckWin(int numOfPlanks, int player)
    {
        if(numOfPlanks >= WinningAmountOfPlanks)
        {
            winningPlayer = player;
            GameOver(true);
        }
    }

    public void DuckMusic(bool duck)
    {
        if(duck)
        {
            BackingMusic.volume = 0.025f;
        }
        else
        {
            BackingMusic.volume = 0.1f;
        }
    }

	Vector3 CalculatePlayersCenterPoint()
	{
		Vector3 position = m_currentPlayers[0].transform.position;

		for (int i = 0; i < m_currentPlayers.Count; ++i)
		{
			position = Vector3.Lerp(position, m_currentPlayers[i].transform.position, 0.5f);
		}

		return position;
	}
}
