﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISystem : MonoBehaviour
{
    public static UISystem s_instance;

    public GameObject TitleScreen;
	public GameObject InGameScreen;
    public GameObject WinScreen;
	public NoiseBar[] NoiseBar;

    public Color[] PlayerColours;
    public Image WinBacking;

	private void Awake() {
        s_instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }
	
    void Update()
    {
        if(InGameScreen.activeSelf)
		{
			SharkManager sMan = SharkManager.s_instance;
			if (sMan)
			{
				NoiseBar[0].SetFillFactor(sMan.GetTargetInterest(0) / sMan.SharkInterestThreshold);
				NoiseBar[1].SetFillFactor(sMan.GetTargetInterest(1) / sMan.SharkInterestThreshold);
			}
		}
    }
	
    public void ShowTitleScreen(bool show)
    {
        if(TitleScreen != null)
        {
            TitleScreen.SetActive(show);
        }
	}

	public void ShowGameScreen(bool show)
	{
		if (InGameScreen != null)
		{
			InGameScreen.SetActive(show);
		}
	}

    public void ShowWinScreen(bool show, int winner)
    {
        if (WinScreen != null)
		{
			WinScreen.SetActive(show);
            WinBacking.color = PlayerColours[winner];
		}
    }
}
