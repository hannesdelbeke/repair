﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shark : MonoBehaviour
{
	public Bounds MovementBounds;
	public Vector3 WanderTarget;
	public Rigidbody HuntTarget;
	public float InterestThreshold = 1.0f;
	public float SwitchHuntTargetFactor = 2.0f;
	public float WanderTargetMinProximity = 1.0f;
	public float WanderRotationSpeed = 50.0f;
	public float HuntRotationSpeed = 200.0f;
	public float WanderSpeed = 5.0f;
	public float HuntSpeed = 10.0f;
	public float BiteProximity = 3.0f;
	public float BiteWindUpTime = 0.4f;
	public float BiteDuration = 0.4f;
	public float BiteMoveSpeed = 10.0f;
	public float HuntCooldown = 5.0f;
	public float WanderCooldown = 1.0f;
	public GameObject HuntingVisual;

	private List<float> m_targetInterest;
	private bool m_isHunting;
	private bool m_isInBitingState;
	private bool m_isInBiteAnim;
	private bool m_hasAppliedBiteDamage;
	private float m_timeInBiteWindUp;
	private Vector3 m_biteTarget;
	private float m_timeUntilHuntCooldown;
	private Vector3 m_velocity;
	private float m_timeTilWander;

	public GameObject sharkMesh;
	private Animator _sharkAnimator;

	public AudioSource m_audio;
	
    void Awake()
    {
        _sharkAnimator = sharkMesh.GetComponent<Animator>();
		m_audio = GetComponentInChildren<AudioSource>();

		m_targetInterest = new List<float>();

		for (int i = 0; i < 2; ++i)
		{
			m_targetInterest.Add(0.0f);
		}
	}
	
    void Update()
    {
		if (m_timeUntilHuntCooldown > 0.0f)
		{
			m_timeUntilHuntCooldown = Mathf.Clamp(m_timeUntilHuntCooldown - Time.deltaTime, 0.0f, HuntCooldown);
			SetHunting(false);
		}
		else
		{
			List<Rigidbody> targets = SharkManager.s_instance.GetHuntTargets();
			for (int i = 0; i < targets.Count; ++i)
			{
				if (targets[i] == null)
					return;
					
				// skip if already hunting this target
				if (m_isHunting && targets[i] == HuntTarget)
				{
					m_targetInterest[i] = targets[i].velocity.magnitude * targets[i].mass;

					if (!MovementBounds.Contains(targets[i].transform.position))
						SetHunting(false);

					if (m_targetInterest[i] < InterestThreshold)
					{
						if (m_timeTilWander <= 0.0f)
							m_timeTilWander = WanderCooldown;

						m_timeTilWander -= Time.deltaTime;

						if (m_timeTilWander <= 0.0f)
						{
							m_timeTilWander = WanderCooldown;
							SetHunting(false);
						}
					}
					continue;
				}
				// else see if this is a better target

				if (!MovementBounds.Contains(targets[i].transform.position))
					continue;

				m_targetInterest[i] = targets[i].velocity.magnitude * targets[i].mass;

				if(m_isHunting)
				{
					float huntInterest = HuntTarget.velocity.magnitude * HuntTarget.mass;
					if((m_targetInterest[i] > (huntInterest * SwitchHuntTargetFactor)))
					{
						HuntTarget = targets[i];
						SetHunting(true);
					}
				}
				else if(!m_isHunting && m_targetInterest[i] >= InterestThreshold)
				{
					HuntTarget = targets[i];
					SetHunting(true);
				}
			}
		}

		if (!m_isHunting)
			UpdateWanderTarget();
		else
		{
			if(m_isInBitingState)
			{
				m_timeInBiteWindUp += Time.deltaTime;

				if(m_timeInBiteWindUp >= BiteWindUpTime)
				{
					// Start bite anim
					m_isInBiteAnim = true;

					if (m_timeInBiteWindUp >= BiteWindUpTime + BiteDuration)
					{
						SetHunting(false);
						m_isInBitingState = false;
						m_isInBiteAnim = false;
						m_timeUntilHuntCooldown = HuntCooldown;
					}
				}
			}

			if(m_isHunting && !m_isInBitingState && Vector3.Distance(transform.position, HuntTarget.position) <= BiteProximity)
			{
				m_isInBitingState = true;
				m_timeInBiteWindUp = 0.0f;
				m_biteTarget = HuntTarget.position;
			}

		}

		if(m_isHunting)
		{
			if(!m_audio.isPlaying)
			{
				m_audio.Play();
				GameLogic.s_instance.DuckMusic(true);
			}
		}
		else
		{
			if(m_audio.isPlaying)
			{
				m_audio.Pause();
				GameLogic.s_instance.DuckMusic(false);

			}
		}

		UpdateMovement();
    }

	void UpdateWanderTarget()
	{
		if(Vector3.Distance(transform.position, WanderTarget) <= WanderTargetMinProximity)
		{
			SetNewWanderTarget();
		}
	}

	void SetNewWanderTarget()
	{
		float xValue = Random.Range(MovementBounds.min.x, MovementBounds.max.x);
		float yValue = Random.Range(MovementBounds.min.y, MovementBounds.max.y);
		float zValue = Random.Range(MovementBounds.min.z, MovementBounds.max.z);

		WanderTarget = new Vector3(xValue, yValue, zValue);
	}

	void UpdateMovement()
	{
		if (m_isHunting)
		{
			if(m_isInBitingState)
			{
				if(m_isInBiteAnim)
				{
					m_velocity = Vector3.forward * BiteMoveSpeed * Time.deltaTime;
					transform.Translate(m_velocity);
				}
				// else stay still
			}
			else
			{
				Vector3 flattenedHuntPosition = new Vector3(HuntTarget.position.x, Mathf.Clamp(HuntTarget.position.y, MovementBounds.min.y, MovementBounds.max.y), HuntTarget.position.z);
				Vector3 huntVector = flattenedHuntPosition - transform.position;
				huntVector = Vector3.Normalize(huntVector);
				float turnFactor = Mathf.Clamp((Vector3.Dot(transform.forward, huntVector) * 2), 0.5f, 1.0f);
				huntVector *= (HuntSpeed * Time.deltaTime);
				m_velocity = Vector3.forward * HuntSpeed * Time.deltaTime * turnFactor;
				transform.Translate(m_velocity);
				transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(huntVector), HuntRotationSpeed * Time.deltaTime);
			}
		}
		else
		{
			Vector3 wanderVector = WanderTarget - transform.position;
			wanderVector = Vector3.Normalize(wanderVector);
			float turnFactor = Mathf.Clamp((Vector3.Dot(transform.forward, wanderVector) * 2), 0.5f, 1.0f);
			wanderVector *= (WanderSpeed * Time.deltaTime);
			m_velocity = Vector3.forward * WanderSpeed * Time.deltaTime * turnFactor;
			transform.Translate(m_velocity);
			transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(wanderVector), WanderRotationSpeed * Time.deltaTime);
		}
	}

	public bool IsBiting()
	{
		// trigger insta bite anim		
		_sharkAnimator.SetTrigger("shark_bite"); 

		return m_isInBiteAnim;
	}

	public Vector3 GetVelocity()
	{
		return m_velocity;
	}

	public float GetTargetInterest(int index)
	{
		return m_targetInterest[index];
	}

	void SetHunting(bool hunting)
	{
		m_isHunting = hunting;
		if (HuntingVisual)
			HuntingVisual.SetActive(hunting);

		m_isInBiteAnim = false;
		m_isInBitingState = false;
		m_timeInBiteWindUp = 0.0f;

		if (!hunting)
		{
			for(int i = 0; i < m_targetInterest.Count; ++i)
			{
				m_targetInterest[i] = 0.0f;
			}
		}
	}

	private void OnGUI()
	{
		//GUI.Label(new Rect(10, 10, 150, 50), "Is Hunting: " + m_isHunting.ToString());
		//GUI.Label(new Rect(10, 30, 150, 50), "Is In Bite State: " + m_isInBitingState.ToString());
		//GUI.Label(new Rect(10, 50, 150, 50), "Is In Bite Anim: " + m_isInBiteAnim.ToString());
		//GUI.Label(new Rect(10, 70, 150, 50), "Interest Value: " + m_targetInterest.ToString());
		GUI.Label(new Rect(10, 70, 150, 50), "Time Til Wander: " + m_timeTilWander.ToString());
	}
}
