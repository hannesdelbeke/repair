﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SharkManager : MonoBehaviour
{
	static public SharkManager s_instance;

	public int NumOfSharksToSpawn = 1;
	public GameObject SharkPrefab;
	public Bounds SharkBounds;
	public float SharkInterestThreshold = 50.0f;

	private List<Shark> m_sharks;
	private List<Rigidbody> m_huntTargets;
	private List<float> m_highestTargetInterest;
	
    void Awake()
	{
		if (s_instance == null)
			s_instance = this;

		m_sharks = new List<Shark>();
		m_huntTargets = new List<Rigidbody>();

		GameObject[] gameObjectsInScene = FindObjectsOfType<GameObject>();
		for(int i = 0; i < gameObjectsInScene.Length; ++i)
		{
			if(gameObjectsInScene[i].tag == "Player")
			{
				m_huntTargets.Add(gameObjectsInScene[i].GetComponent<Rigidbody>());
			}
		}

		for (int i = 0; i < NumOfSharksToSpawn; ++i)
			SpawnShark();

		m_highestTargetInterest = new List<float>();
	}

	void OnDestroy()
	{
		if (s_instance == this)
			s_instance = null;
	}

	void Update()
    {
		if(m_highestTargetInterest.Count < 1)
		{
			for (int i = 0; i < m_huntTargets.Count; ++i)
			{
				m_highestTargetInterest.Add(0.0f);
			}
		}

		for (int i = 0; i < m_highestTargetInterest.Count; ++i)
		{
			m_highestTargetInterest[i] = 0.0f;
		}

		foreach (Shark shark in m_sharks)
		{
			shark.InterestThreshold = SharkInterestThreshold; // for testing purposes

			for (int i = 0; i < m_huntTargets.Count; ++i)
			{
				if (shark.GetTargetInterest(i) > m_highestTargetInterest[i])
					m_highestTargetInterest[i] = shark.GetTargetInterest(i);
			}
		}
    }

	void SpawnShark()
	{
		float xValue = Random.Range(SharkBounds.min.x, SharkBounds.max.x);
		float zValue = Random.Range(SharkBounds.min.z, SharkBounds.max.z);

		GameObject sharkGameObject = Instantiate(SharkPrefab, new Vector3(xValue, transform.position.y, zValue), Quaternion.identity, transform);
		Shark newShark = sharkGameObject.GetComponent<Shark>();
		newShark.MovementBounds = SharkBounds;
		newShark.InterestThreshold = SharkInterestThreshold;
		m_sharks.Add(newShark);
	}

	public List<Rigidbody> GetHuntTargets()
	{
		return m_huntTargets;
	}

	public void AddHuntTarget(Rigidbody rigidBody)
	{
		m_huntTargets.Add(rigidBody);
	}

	public float GetTargetInterest(int index)
	{
		return m_highestTargetInterest[index];
	}
}
