﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class charAnim : MonoBehaviour
{
    private Animator _animator;
    [SerializeField] private GameObject _propJoint;
    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _propJoint.SetActive(false);
    }

    public void Swim()
    {
        _animator.SetTrigger("swimming");
        _propJoint.SetActive(false);
    }
    public void SwimHoldPlank()
    {
        _animator.SetTrigger("swimholdplank");
        _propJoint.SetActive(false);
    }
    public void Paddle()
    {
        _animator.SetTrigger("paddle");
        _propJoint.SetActive(true);
    }
}
