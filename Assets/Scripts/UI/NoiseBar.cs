﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoiseBar : MonoBehaviour
{
	public Image FillBar;
	public Vector3 ShakeSize;
	public float ShakeSpeed = 3.0f;

	private Vector3 m_startPosition;
	private float m_fillFactor;
	private float m_timeSinceShake;

    // Start is called before the first frame update
    void Start()
    {
		m_startPosition = transform.position;
	}

    // Update is called once per frame
    void Update()
	{
		m_timeSinceShake += Time.deltaTime;
		FillBar.fillAmount = m_fillFactor;
		Color fillColour = new Color(1.0f, 1.0f - m_fillFactor, 0.0f);
		FillBar.color = fillColour;

		float shakeFactor = Mathf.Clamp01((m_fillFactor - 0.75f) * 4.0f);
		if (shakeFactor <= 0.0f)
		{
			transform.position = m_startPosition;
			m_timeSinceShake = 0.0f;
		}
		else if (m_timeSinceShake >= (1.0f / ShakeSpeed))
		{
			m_timeSinceShake = 0.0f;
			float xShake = ShakeSize.x * shakeFactor;
			float yShake = ShakeSize.y * shakeFactor;
			float xValue = Random.Range(m_startPosition.x - xShake, m_startPosition.x + xShake);
			float yValue = Random.Range(m_startPosition.y - yShake, m_startPosition.y + yShake);
			transform.position = new Vector3(xValue, yValue, m_startPosition.z);
		}
	}

	public void SetFillFactor(float amount)
	{
		m_fillFactor = amount;
	}
}
