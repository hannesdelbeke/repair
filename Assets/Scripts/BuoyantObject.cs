﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BuoyantObject : MonoBehaviour
{
    private Rigidbody m_body;
    public float BuoyancyForce;

    public float Dampning = 0.1f;

    public AnimationCurve DistanceBobDampen;

    public Vector3[] BuoyancyPointsOffsets;

    private List<Vector3> m_forcePositions;
    private List<Vector3> m_forces;
    private List<Vector3> m_nonForcePositions;

    public Transform byoyancyTransformCenter;
    public bool useTransformPoint;

    void Awake()
    {
        m_body = GetComponent<Rigidbody>();
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // hack to update boyancy point with animation for player
        // if (useTransformPoint)
        // {
        //     BuoyancyPointsOffsets = new  Vector3[1];
        //     BuoyancyPointsOffsets[0] =  transform.position - byoyancyTransformCenter.position;
        // }
        // end hack


        m_forcePositions = new List<Vector3>();
        m_nonForcePositions = new List<Vector3>();
        m_forces = new List<Vector3>();

        float distanceFromWater = Mathf.Abs(Water.Instance.GetDistanceFromWaterHeight(transform.position));





        // hack to update boyancy point with animation for player
        if (useTransformPoint)
        {
            Vector3 pos = byoyancyTransformCenter.position;

            //be Buoyant
            if(!Water.Instance.IsAboveWater(pos))
            {
                Vector3 force = Vector3.up * BuoyancyForce;
                force *= DistanceBobDampen.Evaluate(distanceFromWater);
                m_body.AddForceAtPosition(force, pos);  
                m_forcePositions.Add(pos);
                m_forces.Add(force);
            }
            else
            {
                m_nonForcePositions.Add(pos);
            }
        }
        
        else
        for(int i = 0; i < BuoyancyPointsOffsets.Length; i++)
        {
            Vector3 pos = transform.position + transform.TransformDirection(BuoyancyPointsOffsets[i]);

            //be Buoyant
            if(!Water.Instance.IsAboveWater(pos))
            {
                Vector3 force = Vector3.up * BuoyancyForce;
                force *= DistanceBobDampen.Evaluate(distanceFromWater);
                m_body.AddForceAtPosition(force, pos);  
                m_forcePositions.Add(pos);
                m_forces.Add(force);
            }
            else
            {
                m_nonForcePositions.Add(pos);
            }
        }
        
        //Add Dampning;
        Vector3 dampningForce = -m_body.velocity * m_body.mass * Dampning;
        m_body.AddForce(dampningForce);


    }

    private void OnDrawGizmos() {
        if(!Application.isPlaying)
        {
            for(int i = 0; i < BuoyancyPointsOffsets.Length; i++)
            {
                if(Water.Instance == null)
                {
                    Gizmos.color = Color.green;
                }
                else
                {
                    //be Buoyant
                    if(!Water.Instance.IsAboveWater(transform.position + BuoyancyPointsOffsets[i]))
                    {
                        Gizmos.color = Color.red; 
                    }
                    else
                    {
                        Gizmos.color = Color.green;
                    }
                }
                Gizmos.DrawSphere(transform.position + BuoyancyPointsOffsets[i], 0.1f);

            }
        }
        else
        {
            Gizmos.color = Color.red;
            for(int j = 0; j < m_forcePositions.Count; j++)
            {
                Gizmos.DrawSphere(m_forcePositions[j], 0.25f);
                Gizmos.DrawLine(m_forcePositions[j], m_forcePositions[j] + m_forces[j]);
            }

            Gizmos.color = Color.green;
            for(int j = 0; j < m_nonForcePositions.Count; j++)
            {
                Gizmos.DrawSphere(m_nonForcePositions[j], 0.25f);
            }
        }
    }
}
