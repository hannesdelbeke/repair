﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Water : MonoBehaviour
{
    public Vector2 DebugSize;
    public float DebugSpacing = 1.0f;

    public static Water Instance { get { return m_instance; } private set { }}
    private static Water m_instance;

    public float WaterHeight;
    Vector3[] newVertices;
    Vector2[] newUV;
    int[] newTriangles;

    void Start()
    {

    }
    void Awake()
    {
        m_instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if(Application.isPlaying)
        {
            Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
            Vector3[] vertices = mesh.vertices;

            for (var i = 0; i < vertices.Length; i++)
            {
                vertices[i] = new Vector3(vertices[i].x, GetWaterHeightAtPosition(vertices[i]), vertices[i].z);
            }

            mesh.vertices = vertices;
        }
    }

    public float GetWaterHeightAtPosition(Vector3 pos)
    {
        return WaterHeight + (Mathf.Sin((pos.x + Time.time) / 2.0f) * 0.5f) + (Mathf.Sin((pos.z+  Time.time) / 2.0f) * 0.5f);

    }

    public float GetDistanceFromWaterHeight(Vector3 pos)
    {
        return pos.y - GetWaterHeightAtPosition(pos);
    }

    public bool IsAboveWater(Vector3 pos)
    {
        return pos.y > GetWaterHeightAtPosition(pos);
    }

    void OnDrawGizmos()
    {
        if(DebugSize == Vector2.zero)
        {
            return;
        }
        Gizmos.color = Color.blue;
        int spaces = Mathf.FloorToInt(DebugSize.x / DebugSpacing); 
        float xPos = transform.position.x - (DebugSize.x / 2.0f);
        for(int i = 0; i < spaces; i++)
        {
            float yPos = transform.position.z - (DebugSize.y / 2.0f);

            for(int j = 0; j < spaces; j++)
            {
                Vector3 pos = new Vector3(transform.position.x + xPos, 0.0f, transform.position.z + yPos);
                pos.y = GetWaterHeightAtPosition(pos);
                Gizmos.DrawWireSphere(pos, 0.1f);
                yPos += DebugSpacing;
            }
            xPos += DebugSpacing;
        }
    }
}
