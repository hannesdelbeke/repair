﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class PlayerHitDetector : MonoBehaviour
{
	public float TimeBetweenDamage = 1.0f;
	public float VelocityMultiplier = 30.0f;

	private float m_timeSinceLastDamage;

	private void OnCollisionEnter(Collision collision)
	{
		if (m_timeSinceLastDamage < TimeBetweenDamage)
			return;

		if(collision.collider.tag == "Shark")
		{
			Shark shark = collision.collider.GetComponent<Shark>();
			if(shark.IsBiting())
			{
				BroadcastMessage("OnHit", shark.GetVelocity() * VelocityMultiplier);
				m_timeSinceLastDamage = 0.0f;
			}
		}
	}
	
	void Update()
    {
		m_timeSinceLastDamage += Time.deltaTime;
    }
}
