﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaftManager : MonoBehaviour
{
	static public RaftManager s_instance;

	public GameObject RaftPrefab;

	public int NumOfRaftsToSpawn = 10;

	public Bounds SpawnArea;

	public float RaftCombiningProximity = 1.0f;

	private List<Raft> m_rafts;

	public AudioClip[] SoundClips;
	public AudioSource Source;
	
    void Awake()
    {
		if (s_instance == null)
			s_instance = this;

		m_rafts = new List<Raft>();
		Source = GetComponent<AudioSource>();
    }

	void OnDestroy()
	{
		if (s_instance == this)
			s_instance = null;
	}

	void Update()
    {
		//if(GameLogic.s_instance.currentState == GameLogic.GameState.Playing)
		//{
		//	foreach(Raft raft in m_rafts)
		//	{
		//		if(raft.IsPlayerControlled)
		//		{
		//			foreach (Raft otherRaft in m_rafts)
		//			{
		//				if (otherRaft == raft)
		//					continue;
						
		//				if(Vector3.Distance(raft.transform.position, otherRaft.transform.position) <= RaftCombiningProximity)
		//				{
		//					CombineRafts(raft, otherRaft);
		//					return;
		//				}
		//			}
		//		}
		//	}
		//}
    }

	public List<Raft> GetRafts()
	{
		return m_rafts;
	}

	public void ResetRafts()
	{
		for(int i = 0; i < m_rafts.Count; i++)
		{
			if(m_rafts[i] != null)
			{
				Destroy(m_rafts[i]);
			}
		}
		m_rafts = new List<Raft>();

		Raft[] raftsInScene = FindObjectsOfType<Raft>();
		foreach(Raft raft in raftsInScene)
		{
			m_rafts.Add(raft);
		}

		for(int i = 0; i < NumOfRaftsToSpawn; ++i)
		{
			float xPos = Random.Range(SpawnArea.min.x, SpawnArea.max.x);
			float zPos = Random.Range(SpawnArea.min.z, SpawnArea.max.z);

			CreateRaft(new Vector3(xPos, 0.0f, zPos), Vector3.zero);
		}
	}

	public void AddRaft(Vector3 position)
	{
		GameObject newRaft = Instantiate(RaftPrefab, position, Quaternion.identity, transform);
		Raft raftComponent = newRaft.GetComponent<Raft>();
		m_rafts.Add(raftComponent);
	}

	public void CreateRaft(Vector3 position, Vector3 velocity)
	{
		GameObject newRaft = Instantiate(RaftPrefab, position, Quaternion.identity, transform);
		Raft raftComponent = newRaft.GetComponent<Raft>();
		newRaft.GetComponent<Rigidbody>().AddForce(velocity);
		m_rafts.Add(raftComponent);
	}

	public void BreakRaft(Raft raft, Vector3 breakOrigin, Vector3 velocity)
	{
		int planks = raft.Planks + 1;

		for(int i = 0; i < planks; ++i)
		{
			CreateRaft(breakOrigin + (Vector3.left * i), velocity);
		}

		m_rafts.Remove(raft);
		Destroy(raft.gameObject);
	}

	public void CombineRafts(Raft first, Raft second)
	{
		bool keepFirst = first.IsPlayerControlled || first.Planks >= second.Planks;

		if(keepFirst)
		{
			first.Planks += (second.Planks + 1);
			m_rafts.Remove(second);
			Destroy(second.gameObject);
			first.UpdateAppearance();
		}
		else
		{
			second.Planks += (first.Planks + 1);
			m_rafts.Remove(first);
			Destroy(first.gameObject);

			second.UpdateAppearance();
		}

		Source.clip = SoundClips[Random.Range(0, SoundClips.Length - 1)];
		Source.Play();
	}

	private void OnDrawGizmos() {
		Gizmos.color = Color.green;
		Gizmos.DrawWireCube(SpawnArea.center, SpawnArea.size);
	}
}
