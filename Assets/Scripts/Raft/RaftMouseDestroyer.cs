﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaftMouseDestroyer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButton(0))
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hitInfo;
			if(Physics.Raycast(ray, out hitInfo, 100.0f))
			{
				if (hitInfo.collider.tag == "Raft")
				{
					Raft hitRaft = hitInfo.collider.GetComponent<Raft>();
					if(hitRaft)
					{
						RaftManager.s_instance.BreakRaft(hitRaft, new Vector3(hitInfo.point.x, 0.0f, hitInfo.point.z), Vector3.zero);
					}
				}
			}
		}
    }
}
