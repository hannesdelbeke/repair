﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
public class Raft : MonoBehaviour
{
	public int Planks;
	public bool IsPlayerControlled;
	public GameObject[] RaftModels;
	public GameObject HandIcon;

	public int RaftModelForStanding;

	private Rigidbody m_rigidBody;
	private BuoyantObject m_buoyancy;

	public UnityEvent m_onCombine;

    void Awake()
    {
		Planks = 0;
		UpdateAppearance();
		IsPlayerControlled = false;
		m_rigidBody = GetComponent<Rigidbody>();
		m_buoyancy = GetComponent<BuoyantObject>();
		
		if (m_onCombine == null)
            m_onCombine = new UnityEvent();
    }
	
    void Update()
    {
		
	}

	public void AddPlank()
	{
		++Planks;
		UpdateAppearance();
	}

	public void RemovePlank()
	{
		--Planks;
		UpdateAppearance();
	}

	public void UpdateAppearance()
	{
		int prefabIndex = Mathf.Min(Planks, RaftModels.Length - 1);
		for(int i = 0; i < RaftModels.Length; ++i)
		{
			RaftModels[i].SetActive(i == prefabIndex);
		}
		m_onCombine.Invoke();
	}

	public void OnGrabbed(bool grabbed)
	{
		m_rigidBody.isKinematic = grabbed;
		m_buoyancy.enabled = !grabbed;
		IsPlayerControlled = grabbed;
	}
}
