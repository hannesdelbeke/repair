%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: shark_avatar_mask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: shark:jointRoot
    m_Weight: 1
  - m_Path: shark:jointRoot/shark:jointCenter
    m_Weight: 1
  - m_Path: shark:jointRoot/shark:jointCenter/shark:joint7
    m_Weight: 1
  - m_Path: shark:jointRoot/shark:jointCenter/shark:joint7/shark:joint8
    m_Weight: 1
  - m_Path: shark:jointRoot/shark:jointCenter/shark:joint7/shark:joint8/shark:jointHead
    m_Weight: 1
  - m_Path: shark:jointRoot/shark:jointCenter/shark:joint7/shark:joint8/shark:jointHead/shark:jointUnderjaw
    m_Weight: 1
  - m_Path: shark:jointRoot/shark:jointCenter/shark:joint7/shark:joint8/shark:jointHead/shark:jointUnderjaw/shark:joint2
    m_Weight: 1
  - m_Path: shark:jointRoot/shark:jointCenter/shark:joint7/shark:joint8/shark:jointHead/shark:jointUpperjaw
    m_Weight: 1
  - m_Path: shark:jointRoot/shark:jointCenter/shark:joint7/shark:joint8/shark:jointHead/shark:jointUpperjaw/shark:jointUpperjaw2
    m_Weight: 1
  - m_Path: shark:jointRoot/shark:jointCenter/shark:joint7/shark:joint8/shark:jointHead/shark:jointUpperjaw/shark:jointUpperjaw2/shark:jointUpperjaw3
    m_Weight: 1
  - m_Path: shark:jointRoot/shark:jointCenter/shark:joint10
    m_Weight: 1
  - m_Path: shark:jointRoot/shark:jointCenter/shark:joint10/shark:joint11
    m_Weight: 1
  - m_Path: shark:jointRoot/shark:jointCenter/shark:joint10/shark:joint11/shark:joint12
    m_Weight: 1
  - m_Path: shark:jointRoot/shark:jointCenter/shark:joint10/shark:joint11/shark:joint12/shark:joint13
    m_Weight: 1
  - m_Path: shark:jointRoot/shark:jointCenter/shark:joint10/shark:joint11/shark:joint12/shark:joint13/shark:joint14
    m_Weight: 1
  - m_Path: shark:jointRoot/shark:jointCenter/shark:joint10/shark:joint11/shark:joint12/shark:joint13/shark:joint15
    m_Weight: 1
  - m_Path: shark:shark
    m_Weight: 1
  - m_Path: shark:teethLower
    m_Weight: 1
  - m_Path: shark:teethUpper
    m_Weight: 1
