﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grabber : MonoBehaviour
{
    bool m_holdingPlank;
    Raft m_currentRaft;
    charAnim anim;

    public bool debugHit;

    public Transform GrabberTransform;
    public Transform RaftTransform;
	public float GrabProximity = 3.0f;
	public int PlayerNumber = 1;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<charAnim>();
    }

    // Update is called once per frame
    void Update()
    {
        if(debugHit)
        {
            OnHit(Vector3.zero);
            debugHit = false;
        }

		Raft nearestRaft = null;
		foreach (Raft raft in RaftManager.s_instance.GetRafts())
		{
			if (raft == null)
				continue;

			raft.HandIcon.SetActive(false);
			if (raft.IsPlayerControlled)
				continue;

			float distanceToRaft = Vector3.Distance(raft.transform.position, transform.position);
			if (distanceToRaft <= GrabProximity)
			{
				if (nearestRaft == null)
				{
					nearestRaft = raft;
					continue;
				}
				else if (Vector3.Distance(nearestRaft.transform.position, transform.position) > distanceToRaft)
				{
					nearestRaft = raft;
				}
			}
		}

		if (nearestRaft != null)
			nearestRaft.HandIcon.SetActive(true);

		if (Input.GetButtonDown("Jump" + PlayerNumber.ToString()) && nearestRaft != null)
        {
			if (!m_holdingPlank)
				GrabPlank(nearestRaft);
			else
				RaftManager.s_instance.CombineRafts(m_currentRaft, nearestRaft);
		}
    }

    void GrabPlank(Raft raft)
    {
		raft.transform.SetParent(this.transform);
		raft.transform.position = GrabberTransform.position;
		raft.transform.rotation = GrabberTransform.rotation;
        m_currentRaft = raft;
        m_currentRaft.OnGrabbed(true);
		m_currentRaft.HandIcon.SetActive(false);
        m_holdingPlank = true;
        m_currentRaft.m_onCombine.AddListener(UpdateRaft);
        UpdateRaft();
    }

    void DropPlank()
    {
        m_currentRaft.transform.SetParent(null);
        m_holdingPlank = false;
        m_currentRaft.OnGrabbed(false);
        m_currentRaft.m_onCombine.RemoveAllListeners();
        m_currentRaft = null;
        UpdateRaft();
    }
    
    void UpdateRaft()
    {
        GameLogic.s_instance.CheckWin(m_currentRaft != null ? m_currentRaft.Planks : 0, PlayerNumber);

        if(m_currentRaft != null && m_currentRaft.Planks >= m_currentRaft.RaftModelForStanding)
        {
            anim.Paddle();
            m_currentRaft.transform.position = RaftTransform.position;
        }
        else if(m_currentRaft != null)
        {
            anim.SwimHoldPlank();
        }
        else
        {
            anim.Swim();
        }
    }

    void OnHit(Vector3 velocity)
    {
		if (m_currentRaft)
		{
			RaftManager.s_instance.BreakRaft(m_currentRaft, m_currentRaft.transform.position, velocity);
			DropPlank();
		}
    }
}
